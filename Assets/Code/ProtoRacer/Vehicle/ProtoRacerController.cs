﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvent;
public class ProtoRacerController : MonoBehaviour
{
    private Rigidbody PhysicsBody;
    public RacerControl RacerInput;
    public ProtoRacerConfiguration ProtoRacerConfiguration;

    [Header("Racer Stats")]
    public float CurrentSpeed;
    public float DriveForce;			    //The force that the engine generates
    public float Handling;
    public float BrakingVelFactor;       //The percentage of velocty the ship maintains when braking
    public float AngleOfRoll = 10f;			    //The angle that the ship "banks" into a turn 
    public float DriftFactor;
    public float Durability;
    public float Armor = 1f;
    public bool Grounded;

    [Header("Hover Settings")]
    public PIDController HoverPID;
    public float HoverForce = 100f;
    public float HoverHeight = 0.25f;
    public float MaxGroundDist = 5f;
    public LayerMask TrackLayerMask;

    [Header("Physics Settings")]
    public Transform RacerBody;                 // A reference to the ship's body, this is for cosmetics
    public float TerminalVelocity;       // The max speed the ship can go
    public float HoverGravity = 20f;            // The gravity applied to the racer while it is on the ground
    public float FallGravity = 80f;			    // The gravity applied to the racer while it is falling
    float drag;								    //The air resistance the ship recieves in the forward direction

    private const float AccelerationMultiplier = 100f;
    private const float HandlingMultiplier = 100f;
    private ProtoRacerCollison protoRacerCollison;
    public FloatValueChange OnDurabilityChange;

    public bool RacerLocked;

    void Start()
    {
        PhysicsBody = GetComponent<Rigidbody>();

        ProtoRacerConfiguration.InitializeVariables(this);
        protoRacerCollison = GetComponent<ProtoRacerCollison>();
        protoRacerCollison.OnProtoRacerCollision.AddListener(HandleCollision);
        drag = DriveForce / TerminalVelocity;

    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        if (RacerLocked)
            return;
        CurrentSpeed = Vector3.Dot(PhysicsBody.velocity, transform.forward);
        Hover();
        DriveRacer();
    }
    private void Hover()
    {
        //This variable will hold the "normal" of the ground. Think of it as a line
        //the points "up" from the surface of the ground
        Vector3 groundNormal;

        //Calculate a ray that points straight down from the ship
        Ray ray = new Ray(transform.position, -transform.up);

        //Declare a variable that will hold the result of a raycast
        RaycastHit hitInfo;

        //Determine if the ship is on the ground by Raycasting down and seeing if it hits 
        //any collider on the whatIsGround layer
        Grounded = Physics.Raycast(ray, out hitInfo, MaxGroundDist, TrackLayerMask);

        if (Grounded)
        {
            //...determine how high off the ground it is...
            float height = hitInfo.distance;
            //...save the normal of the ground...
            groundNormal = hitInfo.normal.normalized;
            //...use the PID controller to determine the amount of hover force needed...
            float forcePercent = HoverPID.Seek(HoverHeight, height);

            //...calulcate the total amount of hover force based on normal (or "up") of the ground...
            Vector3 force = groundNormal * HoverForce * forcePercent;
            //...calculate the force and direction of gravity to adhere the ship to the 
            //track (which is not always straight down in the world)...
            Vector3 gravity = -groundNormal * HoverGravity * height;

            //...and finally apply the hover and gravity forces
            PhysicsBody.AddForce(force, ForceMode.Acceleration);
            PhysicsBody.AddForce(gravity, ForceMode.Acceleration);
        }
        else
        {
            //...use Up to represent the "ground normal". This will cause our ship to
            //self-right itself in a case where it flips over
            groundNormal = Vector3.up;

            //Calculate and apply the stronger falling gravity straight down on the ship
            Vector3 gravity = -groundNormal * FallGravity;
            PhysicsBody.AddForce(gravity, ForceMode.Acceleration);
        }
        //Calculate the amount of pitch and roll the ship needs to match its orientation
        //with that of the ground. This is done by creating a projection and then calculating
        //the rotation needed to face that projection
        Vector3 projection = Vector3.ProjectOnPlane(transform.forward, groundNormal);
        Quaternion rotation = Quaternion.LookRotation(projection, groundNormal);

        //Move the ship over time to match the desired rotation to match the ground. This is 
        //done smoothly (using Lerp) to make it feel more realistic
        PhysicsBody.MoveRotation(Quaternion.Lerp(PhysicsBody.rotation, rotation, Time.deltaTime * 10f));

        //Calculate the angle we want the ship's body to bank into a turn based on the current rudder.
        //It is worth noting that these next few steps are completetly optional and are cosmetic.
        //It just feels so darn cool
        float angle = AngleOfRoll * -RacerInput.SteerDirection;

        //Calculate the rotation needed for this new angle
        Quaternion bodyRotation = transform.rotation * Quaternion.Euler(0f, 0f, angle);
        //Finally, apply this angle to the ship's body
        RacerBody.rotation = Quaternion.Lerp(RacerBody.rotation, bodyRotation, Time.deltaTime * 10f);

    }
    void DriveRacer()
    {
        
        //Calculate the yaw torque based on the rudder and current angular velocity
        float rotationTorque = (RacerInput.SteerDirection) - PhysicsBody.angularVelocity.y;
        //Apply the torque to the ship's Y axis
        //PhysicsBody.AddRelativeTorque(0f, rotationTorque * Handling, 0f, ForceMode.VelocityChange);
        PhysicsBody.AddRelativeTorque(Vector3.up * RacerInput.SteerDirection * Handling, ForceMode.Force);

        //Calculate the current sideways speed by using the dot product. This tells us
        //how much of the ship's velocity is in the "right" or "left" direction
        float sidewaysSpeed = Vector3.Dot(PhysicsBody.velocity, transform.right);

        //Calculate the desired amount of friction to apply to the side of the vehicle. This
        //is what keeps the ship from drifting into the walls during turns. 

        //Vector3 sideFriction = -transform.right * (sidewaysSpeed / Time.fixedDeltaTime);

        //If you want to add drifting to the game, divide Time.fixedDeltaTime by some amount
        float driftValue = DriftFactor / Time.fixedDeltaTime;
        Vector3 sideFriction = -transform.right * (sidewaysSpeed / driftValue);

        //Finally, apply the sideways friction
        PhysicsBody.AddForce(sideFriction, ForceMode.Acceleration);

        //If not propelling the ship, slow the ships velocity
        //if (RacerInput.Thruster <= 0f)
        //    PhysicsBody.velocity *= slowingVelFactor;

        //Braking or driving requires being on the ground, so if the ship
        //isn't on the ground, exit this method
        if (!Grounded)
            return;

        //If the ship is braking, apply the braking velocty reduction
        if (RacerInput.IsBraking)
            PhysicsBody.velocity *= BrakingVelFactor;

        //Calculate and apply the amount of propulsion force by multiplying the drive force
        //by the amount of applied thruster and subtracting the drag amount
        float propulsion = DriveForce * RacerInput.Thruster - drag * Mathf.Clamp(CurrentSpeed, 0f, TerminalVelocity);
        PhysicsBody.AddForce(transform.forward * propulsion, ForceMode.Acceleration);
    }
    void OnCollisionStay(Collision collision)
    {
        //If the ship has collided with an object on the Wall layer...
        if (collision.gameObject.layer == LayerMask.NameToLayer("Track"))
        {
            //...calculate how much upward impulse is generated and then push the vehicle down by that amount 
            //to keep it stuck on the track (instead up popping up over the wall)
            Vector3 upwardForceFromCollision = Vector3.Dot(collision.impulse, transform.up) * transform.up;
            PhysicsBody.AddForce(-upwardForceFromCollision, ForceMode.Impulse);

            //Vector3 horizontalForceFromCollision = Vector3.Dot(collision.relativeVelocity, transform.right) * transform.right * 0.1f;
            //PhysicsBody.AddForce(-horizontalForceFromCollision, ForceMode.Impulse);
        }
    }

    public float GetSpeedPercentage()
    {
        //Returns the total percentage of speed the ship is traveling
        return PhysicsBody.velocity.magnitude / TerminalVelocity;
    }

    public void GroundCheck()
    {
        if (!Physics.Raycast(transform.position, -transform.up, out RaycastHit rayHit, HoverHeight, TrackLayerMask))
        {
            PhysicsBody.AddForceAtPosition(-Vector3.up * (1.0f - (rayHit.distance / HoverHeight)), transform.position);
            Grounded = false;
        }
        else
        {
            Grounded = true;
        }
    }

    public void HandleCollision(float damageValue)
    {
        float amount = damageValue / Armor;
        Debug.Log("Racer Damaged for " + amount);
        Durability -= amount;
        OnDurabilityChange.Invoke(Durability);
        if (Durability <= 0)
        {
            if (RacerInput.IsPlayer)
            {
                GameManager.Instance.RestartLevel();
            }
        }
    }
}
