﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="newProtRacerConfig", menuName = "ProtoRacer/Racer Vehicle Config")]
public class ProtoRacerConfiguration : ScriptableObject
{
    [Header("Vehicle Settings")]
    [Range(ProtoRacerMinMaxStat.DriveForceMin, ProtoRacerMinMaxStat.DriveForceMax)]
    public float DriveForce;

    [Range(ProtoRacerMinMaxStat.HandlingMin, ProtoRacerMinMaxStat.HandlingMax)]
    public float Handling;

    [Range(0, 1)]
    public float BrakingVelocityFactor;

    [Range(ProtoRacerMinMaxStat.DriftFactorMin, ProtoRacerMinMaxStat.DriftFactorMax)]
    public float DriftFactor;
    [Range(ProtoRacerMinMaxStat.DurabilityMin, ProtoRacerMinMaxStat.DurabilityMax)]
    public float Durability;

    //[Header("Hover Settings")]
    //public float HoverForce;
    //public float HoverHeight;
    //public float MaxGroundDistance;
    [Header("Physics Settings")]
    [Range(ProtoRacerMinMaxStat.TerminalVelocityMin, ProtoRacerMinMaxStat.TerminalVelocityMax)]
    public float TerminalVelocity;
    //public float HoverGravity;
    //public float FallGravity;

    public void InitializeVariables(ProtoRacerController protoRacerController)
    {
        protoRacerController.DriveForce = DriveForce;
        protoRacerController.Handling = Handling;
        protoRacerController.BrakingVelFactor = BrakingVelocityFactor;
        protoRacerController.DriftFactor = DriftFactor;
        protoRacerController.Durability = Durability;
        protoRacerController.TerminalVelocity = TerminalVelocity;
    }
}
