﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ProtoRacerMinMaxStat
{
    public const float DriveForceMin = 0f;
    public const float DriveForceMax = 20f;

    public const float HandlingMin = 1;
    public const float HandlingMax = 3;

    public const float BrakingVelocityFactorMin = 0;
    public const float BrakingVelocityFactorMax = 1;

    public const float DriftFactorMin = 0.01f;
    public const float DriftFactorMax = 0.03f;

    public const float DurabilityMin = 0f;
    public const float DurabilityMax = 1000f;

    //public static float HoverForceMin;
    //public static float HoverForceMax;

    //public static float HoverHeightMin;
    //public static float HoverHeightMax;

    //public static float MaxGroundDistanceMin;
    //public static float MaxGroundDistanceMax;

    public const float TerminalVelocityMin = 10;
    public const float TerminalVelocityMax = 100;

    //public static float HoverGravityMin;
    //public static float HoverGravityMax;

    //public static float FallGravityMin;
    //public static float FallGravityMax;
}
