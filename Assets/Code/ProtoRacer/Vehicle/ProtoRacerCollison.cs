﻿using ProtoRacerEvents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtoRacerCollison : MonoBehaviour
{
    public ProtoRacerCollisionEvent OnProtoRacerCollision;
    private void OnCollisionEnter(Collision collision)
    {
        OnProtoRacerCollision.Invoke(collision.impulse.magnitude);
    }
}
