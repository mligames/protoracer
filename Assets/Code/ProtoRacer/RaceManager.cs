﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceManager : Singleton<RaceManager>
{
    public ProtoRacerController[] CurrentRacers;
    public float StartingCount;
    public float CountdownTimer;
    public float DisplayLingerTime;
    public bool PreRaceCheckComplete;
    public bool RaceInProgress;
    // Start is called before the first frame update
    void Start()
    {
        PreRaceCheckComplete = false;
        RaceInProgress = false;
        StartCoroutine(PreRaceSetUpRoutine());
    }
    // Update is called once per frame
    void Update()
    {
        if (PreRaceCheckComplete && !RaceInProgress)
        {
            CountdownTimer = Mathf.Clamp(CountdownTimer -= Time.deltaTime, 0f, StartingCount);
            StartRacePanel.Instance.UpdateTimeDisplay(CountdownTimer);
            if(CountdownTimer <= 0)
            {
                StartCoroutine(StartRaceRoutine());
                RaceInProgress = true;
            }
        }
    }
    IEnumerator PreRaceSetUpRoutine()
    {
        CurrentRacers = FindObjectsOfType<ProtoRacerController>();

        foreach (var racer in CurrentRacers)
        {
            racer.RacerLocked = true;
        }

        CountdownTimer = StartingCount;

        yield return null;

        StartRacePanel.Show();
        StartRacePanel.Instance.TimeDisplay.text = StartingCount.ToString("F2");
        PreRaceCheckComplete = true;
    }

    IEnumerator StartRaceRoutine()
    {
        CurrentRacers = FindObjectsOfType<ProtoRacerController>();
        foreach (var racer in CurrentRacers)
        {
            racer.RacerLocked = false;
        }
        yield return null;
        StartRacePanel.Instance.CountdownComplete();
    }
}
