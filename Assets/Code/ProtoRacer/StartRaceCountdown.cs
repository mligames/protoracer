﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class StartRaceCountdown : MonoBehaviour
{
    public float countdownAmount;
    public float countdownTimer;
    public float displayLingerTime;
    public UnityEvent StartRace;
    // Start is called before the first frame update
    void Start()
    {
        if (StartRace == null)
            StartRace = new UnityEvent();
        countdownTimer = countdownAmount;

        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (countdownAmount > 0f)
        {
            countdownTimer = Mathf.Clamp(countdownTimer -= Time.deltaTime, 0f, countdownAmount);
            //timeDisplay.text = countdownTimer.ToString("F2");
        }
        if(countdownTimer <= 0f)
        {
            StartRace.Invoke();
            //messageDisplay.text = "GO!";
            //timeDisplay.text = "";
            StartCoroutine(DisplayLingerTime());
        }

    }
    IEnumerator DisplayLingerTime()
    {
        yield return new WaitForSeconds(displayLingerTime);
        gameObject.SetActive(false);
    }
}
