﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchControl : Selectable
{
    public bool debugging;
    public KeyCode debugKey;
    public enum ControlDirection { Left, Right }
    [SerializeField] public ControlDirection controlDirection;
    public bool pressed;

    protected override void Start()
    {
        base.Start();
        switch (controlDirection)
        {
            case ControlDirection.Left:
                FindObjectOfType<PlayerRacerControl>().LeftTouch = this;
                break;
            case ControlDirection.Right:
                FindObjectOfType<PlayerRacerControl>().RightTouch = this;
                break;
        }
    }

    private void Update()
    {
        if (debugging)
        {
            if (Input.GetKey(debugKey))
            {
                pressed = true;
            }
            else
            {
                pressed = false;
            }
            return;
        }

        if (IsPressed())
            pressed = true;
        else
            pressed = false;
    }
    public int GetInputValue()
    {
        int value = 0;
        switch (controlDirection)
        {
            case ControlDirection.Left:
                value = -1;
                break;
            case ControlDirection.Right:
                value = 1;
                break;
        }
        return value;
    }
}
