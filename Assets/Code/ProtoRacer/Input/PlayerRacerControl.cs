﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRacerControl : RacerControl
{
    public KeyCodeVariable SteerLeft;
    public KeyCodeVariable SteerRight;

    public TouchControl LeftTouch;
    public TouchControl RightTouch;

    [SerializeField] private float steeringDirection;
    public override float SteerDirection { get => steeringDirection; set => steeringDirection = value; }

    [SerializeField] private float thruster;
    public override float Thruster { get => thruster; set => thruster = value; }

    [SerializeField] private bool isBraking;
    public override bool IsBraking { get => isBraking; set => isBraking = value; }

    private void Update()
    {
        Thruster = 1f;
        SteerDirection = 0;
        if (LeftTouch.pressed)
        {
            SteerDirection += LeftTouch.GetInputValue();
        }
        if (RightTouch.pressed)
        {
            SteerDirection += RightTouch.GetInputValue();
        }

        if (LeftTouch.pressed && RightTouch.pressed)
        {
            SteerDirection = 0;
            IsBraking = true;
        }
        else
        {
            IsBraking = false;
            Thruster = 1f;
        }
    }
}
