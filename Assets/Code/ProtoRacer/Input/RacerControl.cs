﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RacerControl : MonoBehaviour
{
    public bool IsPlayer;
    public abstract float Thruster { get; set; }
    public abstract bool IsBraking { get; set; }
    public abstract float SteerDirection { get; set; }
}
