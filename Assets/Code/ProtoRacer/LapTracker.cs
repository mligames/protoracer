﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapTracker : MonoBehaviour
{
    public int CurrentLap;
    public float CurrentLapTime = 0f;
    public List<LapTime> LapTimesList;
    private void Update()
    {
        CurrentLapTime += Time.deltaTime;
    }
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name != "FinishLine")
            return;
        CurrentLap++;

        if (CurrentLap < 1)
        {
            CurrentLapTime = 0f;
            return;
        }

        LapTime lapTime = new LapTime(CurrentLap, CurrentLapTime);

        LapTimesList.Add(lapTime);
    }
}
