﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ProtoRacerEvents
{
    [Serializable] public class ProtoRacerCollisionEvent : UnityEvent<float> { }
}
