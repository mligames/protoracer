﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Should the camera follow the target
    [SerializeField] bool follow = false;
    public bool Follow { get { return follow; } set { follow = value; } }

    // Current target
    [SerializeField] Transform target;

    // ALl possible targets
    [SerializeField] Transform[] targets;

    // Offset from the target position
    [SerializeField] Vector3 offset;

    // Camera speeds
    [Range(0, 10)]
    [SerializeField] float lerpPositionMultiplier = 1f;
    [Range(0, 360)]
    [SerializeField] float lerpRotationMultiplier = 1f;


    private void Start()
    {
        transform.parent = null;
    }
    void FixedUpdate()
    {
        // If we don't follow or target is null return
        if (!follow || target == null) return;

        // normalise velocity so it doesn't jump too far
        //this.rb.velocity.Normalize();

        // Save transform localy
        Quaternion curRot = transform.rotation;
        Vector3 tPos = target.position + target.TransformDirection(offset);

        // Look at the target
        transform.LookAt(target);

        // Keep the camera above the target y position
        if (tPos.y < target.position.y)
        {
            tPos.y = target.position.y;
        }

        // Set transform with lerp
        transform.position = Vector3.Lerp(transform.position, tPos, Time.fixedDeltaTime * lerpPositionMultiplier);
        transform.rotation = Quaternion.Lerp(curRot, transform.rotation, Time.fixedDeltaTime * lerpRotationMultiplier);

        // Keep camera above the y:0.5f to prevent camera going underground
        if (transform.position.y < 0.5f)
        {
            transform.position = new Vector3(transform.position.x, 0.5f, transform.position.z);
        }

    }
}
