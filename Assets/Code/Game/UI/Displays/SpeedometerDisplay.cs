﻿using GameEnum;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedometerDisplay : Singleton<SpeedometerDisplay>
{
    public SpeedType speedType;
    public Text speedometerReadout;
    public ProtoRacerController ProtoRacerController;
    private void Start()
    {
        ProtoRacerController = FindObjectOfType<PlayerRacerControl>().GetComponent<ProtoRacerController>();
    }
    private void Update()
    {
        ConvertVelocity();
    }
    public void ConvertVelocity()
    {
        float speed = ProtoRacerController.CurrentSpeed;
        switch (speedType)
        {
            case SpeedType.MPH:

                speed *= 2.23693629f;
                speedometerReadout.text = "MPH: " + speed.ToString("F0");
                break;

            case SpeedType.KPH:
                speed *= 3.6f;
                speedometerReadout.text = "KPH: " + speed.ToString("F0");
                break;
        }
    }
}
