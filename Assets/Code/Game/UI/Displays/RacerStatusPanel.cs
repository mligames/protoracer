﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RacerStatusPanel : MonoBehaviour
{
    public ProtoRacerController ProtoRacerController;
    public Slider DurabliltySlider;
    // Start is called before the first frame update
    void Start()
    {
        ProtoRacerController = FindObjectOfType<PlayerRacerControl>().GetComponent<ProtoRacerController>();

        DurabliltySlider.minValue = 0;
        DurabliltySlider.maxValue = ProtoRacerController.Durability;
        DurabliltySlider.value = ProtoRacerController.Durability;
    }
}
