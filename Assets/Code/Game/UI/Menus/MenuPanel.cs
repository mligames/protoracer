﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MenuPanel<T> : Menu<T> where T: MenuPanel<T>
{
    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }
}
