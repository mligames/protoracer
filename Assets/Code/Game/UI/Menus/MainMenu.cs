﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MenuPanel<MainMenu>
{
    public void OnPlayPressed()
    {
        Hide();
        Destroy(this.gameObject); // This menu does not automatically destroy itself
        GameManager.Instance.StartGame();
    }

    public void OnOptionsPressed()
    {
        OptionsMenu.Show();
    }

    public void OnQuitPressed()
    {
        GameManager.Instance.QuitGame();
    }
}
