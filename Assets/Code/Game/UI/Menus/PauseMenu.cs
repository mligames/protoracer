﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MenuPanel<PauseMenu>
{
    private void OnEnable()
    {
        Time.timeScale = 0;
    }
    private void OnDisable()
    {
        Time.timeScale = 1;
    }
    public override void OnBackPressed()
    {
        base.OnBackPressed();
    }
    public void OnRetryPressed()
    {
        Hide();
        Destroy(this.gameObject); // This menu does not automatically destroy itself
        GameManager.Instance.RestartLevel();
    }
    public void OnQuitPressed()
    {
        Hide();
        Destroy(this.gameObject); // This menu does not automatically destroy itself
        GameManager.Instance.QuitToTitle();
    }
}
