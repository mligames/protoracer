﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MenuPanel<OptionsMenu>
{
    private void OnEnable()
    {

    }
    private void OnDisable()
    {

    }
    public override void OnBackPressed()
    {
        base.OnBackPressed();
    }
    public void OnVehicleConfigPressed()
    {
        VehicleConfigMenu.Show();
    }

}
