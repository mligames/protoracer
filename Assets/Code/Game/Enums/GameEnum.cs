﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameEnum
{
    [Serializable] public enum GameState { RUNNING, PAUSED, SCENECHANGE, GAMEOVER }

    [Serializable] public enum SpeedType { KPH, MPH}

}
